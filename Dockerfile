FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
ENV PYTHONPATH=/usr/src/app
ENV SQLALCHEMY_DATABASE_URI="postgresql://user:somestrongpassword@172.17.0.2:5432/users"
ENTRYPOINT [ "python", "roverqaz_simple_api" ]